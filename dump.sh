#!/bin/bash

VERSION='1.6.4.8'
# DATE_VERSION="$(date +%F)"
DATE_VERSION='2020-03-16'
AUTHOR='Alessandro Selli http://alessandro.route-add.net/'
LICENSE='GPLv3+'
# Se HOSTNAME � impostato, imposta HOST al suo valore. Altrimenti usa quello che restituisce hostname
HOST="${HOSTNAME:-$(hostname 2> /dev/null)}"
  # Togli l'eventuale dominio appeso al nome del nodo
  HOST="${HOST%.[a-zA-Z0-9-]*}"
SERVER='niraya'
DIR='/mnt/dump_backup'
MOUNTDIR=''
DATE="$(date '+%Y-%m-%d')"
LEVEL='0'
# Inizializza FILE e, se HOST � vuoto, inizializzalo a localhost
FILE="${HOST:=localhost}_${DATE}_$LEVEL"
DUMPDATES_FILE="${HOST}_dumpdates"
DUMPDATES="$DIR/$DUMPDATES_FILE"
USER_PROVIDED_DUMPDATES='false'
MYSELF="$(basename -- $0)"
OVERWRITE='no'
VERB='no'
SYNCDD='false'
TEST='no'
TOC='no'
NFS='no'
NFS_AUTOUMOUNT='yes'
NFS_UMOUNT_COUNT=2
NFS_UMOUNT_TMOUT=2
QUITONERR='false'
QUIET=1 # Preimpostato a falso, ossia non quieto
COD=0
ECHOE=''
BOLD=''
NORM=''
COLUMNS=${COLUMNS:=80} # Non lo trovo inizializzato!

# De-utf8-izza LANG, che questo script (e quindi i suoi messaggi) sono scritti
# in ISO-8859-1
LANG="${LANG%\.UTF-8}"
LANG="${LANG%\.UTF8}"
LANG="${LANG%\.utf-8}"
LANG="${LANG%\.utf8}"

# Permetti di espandere gli alias nella shell non interattiva
shopt -s expand_aliases
# Aggiorna i valori di LINES e COLUMNS
shopt -s checkwinsize
kill -SIGWINCH $$ # In teoria dovrebbe reinizializzare LINES e COLUMNS, ma non succede

alias QEcho='test "$QUIET" -ne 0 && echo$ECHOE'
alias VEcho='test "$VERB" != "no" && echo$ECHOE'
# Potrebbe essere necessario togliere a fmt il parametro --goal=$(($COLUMNS - 1)), si
# verificasse l'errore seguente:
# fmt: unrecognized option '--goal=79'
# fmt di coreutils-8.13-3.5
# coreutils-8.23-4 (Ubuntu 15.0 e Debian 8.3) ce l'hanno
alias format_out='fmt --uniform-spacing --tagged-paragraph --width=$COLUMNS'
# Assume /proc/mounts sia post 2.4.19, se da quel kernel il formato � cambiato.
alias Md_Mounted="awk -v V=\"\$MOUNTDIR\" '\$2 == V {TROVATO = 1 ; exit} END {print TROVATO ? \"yes\" : \"no\"}' /proc/mounts"

# Il comando infocmp (del pacchetto ncurses) serve per ottenere le propriet�
# del terminale.  Se c'�, si usa per fighettizzare i messaggi.  Se non c'�
# pazienza, si va di dumb-terminal.
if command -v infocmp > /dev/null
then if test -n "$(infocmp -1I $TERM | grep 'bold=\\E\[1m,')"
     then ESC="$(echo -en '\033')"
     	  ECHOE=' -e'
	  NORM="${ESC}[0m"
          BOLD="${ESC}[1m"
     fi
fi

for COM in basename dirname hostname dump tr date awk mktemp fmt
do if ! command -v "$COM" > /dev/null
   then QEcho "${BOLD}ERRORE:${NORM} comando \"$COM\" necessario ma non disponibile" >&2
        exit 2
   fi
done

# Dump su un filesystem NFS ha prestazioni dannatamente scadenti (1/20 la
# larghezza di banda di una connessione da 100mbs).  Le operazioni di scrittura
# sono infatti pesantemente penalizzate dalle scritture sincrone che dump
# effettua sempre, indipendentemente dalle opzioni con cui � stato montato il
# filesystem NFS.  Per ovviare a questo problema, non avendo dump un'opzione
# che abiliti la scrittura asincrona, � stato introdotto lo switch -n (NFS) che
# crea una named pipe temporanea in $TMPDIR o in $TMP o in /tmp, mandando in
# esecuzione nello sfondo un cat che legge dalla named pipe e scrive nel file
# di dump sul filesystem NFS, e infine istruisce dump ad effettuare le
# scritture nella named pipe.

# DA FARE:
# 0) controllare, terminato un dump con l'opzione NFS (-n) attivata, che il comando
#    cat associato alla named pipe in cui scrive dump sia terminato;
# 1) gestire la compressione;
# 2) permettere di specificare sulla riga di comando i filesystem di cui
#    {,non} eseguire un dump, invece di farlo automaticamente per tutti i filesystem in
#    /etc/fstab che lo richiedono.

function Esci () {
# Pulizia
# La funzione Esci � usata ogni volta che lo script cessa l'esecuzione, grazie alla
# trap EXIT.  Questo non impedisce allo script di uscire con un codice arbitrario 
# con un'istruzione exit x
if test $# -eq 1
then SIG="SIG${1#SIG}"
     if kill -l | grep -q "$SIG"
     then QEcho "Uscita causa ricezione segnale ${BOLD}$SIG${NORM}" >&2
          ERR=4
     else QEcho "${BOLD}ERRORE:${NORM} uscita anomala: funzione ${BOLD}Esci${NORM} chiamata con parametro: $1 (non � un segnale)" >&2
          ERR=6
     fi
else if test $# -gt 0
     then QEcho "${BOLD}ERRORE:${NORM}Uscita anomala: funzione ${BOLD}Esci${NORM} chiamata con $# parametri" >&2
          ERR=7
     fi
fi
# Se � un test PIPE non esiste, inutile mettere $ECHO davanti a rm
test -p "$PIPE" && rm -f "$PIPE"
COD=$?
if test -n "$MOUNTDIR" -a "$MD_IN_FSTAB" = 'yes' -a "$(Md_Mounted)" = 'no'
then if test "$NFS_AUTOUMOUNT" = 'yes'
     then C=1
          while test $C -le "$NFS_UMOUNT_COUNT"
          do $ECHO sync # Ha senso per i filesystem NFS?
             $ECHO umount "$MOUNTDIR"
             COD=$?
             if test $COD -ne 0
             then QEcho "${BOLD}ERRORE:${NORM} comando umount uscito con codice $COD" >&2
             fi
             C=$((C+1))
             $ECHO sleep "$NFS_UMOUNT_TMOUT"
          done
     else VEcho "Directory \"$MOUNTDIR\" non smontata per esplicita richiesta"
     fi
fi
exit
}

for SIGNAL in EXIT HUP INT ABRT TERM QUIT
do trap Esci "$SIGNAL"
done

for SIGNAL in USR1 USR2
do trap : "$SIGNAL"
done

function Codici () {
cat << EOH | format_out
${BOLD}Codici di errore:${NORM}
${BOLD}1${NORM}: opzione non valida
${BOLD}2${NORM}: non � stato trovato qualche comando necessario
${BOLD}3${NORM}: nessun filesystem di cui eseguire il dump in /etc/fstab
${BOLD}4${NORM}: uscita prematura per ricezione segnale
${BOLD}6${NORM}: uscita con chiamata anomala funzione Esci con primo parametro non nome di segnale
${BOLD}7${NORM}: uscita con chiamata anomala funzione Esci con pi� di un parametro
${BOLD}10${NORM}: fallita verifica che il nome host/server contenga solo i caratteri previsti dalla RFC-1034
${BOLD}15${NORM}: errore interno invocazione procedura controllo nome host/server
${BOLD}20${NORM}: il valore del livello di dump non � numerico
${BOLD}22${NORM}: il valore del livello di dump contiene pi� di tre cifre
${BOLD}25${NORM}: errore interno invocazione procedura controllo numero
${BOLD}26${NORM}: errore interno invocazione procedura gestione controllo procedura controllo numero
${BOLD}27${NORM}: funzione VerificaNumero_err invocata con il primo parametro non numerico
${BOLD}28${NORM}: indicazione numero di secondi attesa tra tentativi di smontaggio NFS errato (valore non numerico o maggiore di 999)
${BOLD}29${NORM}: indicazione numero di tentativi smontaggio NFS errato (valore non numerico o maggiore di 999)
${BOLD}30${NORM}: errato formato data
${BOLD}31${NORM}: data errata per il calendario
${BOLD}35${NORM}: errore invocazione procedura controllo data
${BOLD}36${NORM}: il parametro per MOUNTDIR non indica una directory
${BOLD}37${NORM}: errore invocazione procedura controllo directory
${BOLD}38${NORM}: montaggio di "${BOLD}$MOUNTDIR${NORM}" impossibile: la directory non compare in /etc/fstab 
${BOLD}39${NORM}: montaggio di "${BOLD}$MOUNTDIR${NORM}" fallito
${BOLD}40${NORM}: fallita esecuzione di mktemp
${BOLD}45${NORM}: fallita creazione di named pipe in $TMPDIR
${BOLD}50${NORM}: DIR non indica una directory scrivibile
${BOLD}53${NORM}: la directory di DUMPDATES non indica una directory scrivibile
${BOLD}60${NORM}: directory indicata per il file dumpdates non valida
${BOLD}70${NORM}: comando dump terminato con errore e opzione -e attiva
${BOLD}100+n${NORM}: errori di funzioni interne: n=0 funzione VerificaHostname;
        n=1 funzione VerificaData; n=3 funzione VerificaNumero
EOH
}

function Versione () {
 echo -e "$MYSELF Versione $VERSION del $DATE_VERSION\\nDi $AUTHOR\\nRilasciata sotto licenza $LICENSE"
}

function Help () {
Versione

cat << EOH | format_out

Programma di backup disco su file che usa il comando dump per effettuare backup completi o incrementali o differenziali.

${BOLD}Uso:${NORM} $MYSELF [-c volte_umount] [-d dir] [-D data] [-e] [-f base_file]
     [-F dumpdates] [-h] [-H hostname] [-l l] [-m mount_dir] [-n] [-N] [-o]
     [-p parms] [-q] [-s] [-t] [-T] [-u scad_umount] [-v] [-V] [-x] [filesystem]

Dove (tra parentesi quadre i valori in uso):

${BOLD}volte_umount${NORM} indica il numero di volte che deve essere tentato lo smontaggio (umount) della directory NFS, nel caso in cui ritardi di rete facciano fallire il primo tentativo. Si veda anche il parametro ${BOLD}scad_umount${NORM} ${BOLD}[$NFS_UMOUNT_COUNT]${NORM}
${BOLD}dir${NORM} indica il percorso locale della directory dove va scritto il file di dump e, eventualmente, dove � posto il file dumpdates (switch -F) ${BOLD}[$DIR]${NORM};
${BOLD}data${NORM} indica la data da usare per costruire il nome del file di dump ${BOLD}[$DATE]${NORM}
${BOLD}base_file${NORM} indica la base del nome del file di dump ${BOLD}[$FILE]${NORM} Quella preimpostata � composta da: "${BOLD}\${HOST}_\${DATE}_\${LEVEL}${NORM}". A questa base � poi automaticamente aggiunta la componente "${BOLD}_\$FILESYSTEM.dump${NORM}"
${BOLD}dumpdates${NORM} indica il file dumpdates che deve usare dump per tenere traccia delle operazioni eseguite, con eventuale percorso alternativo ${BOLD}[$DUMPDATES]${NORM};
${BOLD}hostname${NORM} indica il nome del computer che esegue ${MYSELF} ${BOLD}[$HOST]${NORM};
${BOLD}l${NORM} indica il livello di dump che si vuole eseguire, compreso tra 0 e 999 ${BOLD}[$LEVEL]${NORM}$(test -n "$MOUNTDIR" && echo -e "\n${BOLD}mount_dir${NORM} indica la directory da montare per accedere al deposito dei file di dump ${BOLD}[$MOUNTDIR]${NORM}")
${BOLD}scad_umount${NORM} numero di secondi da attendere tra un tentativo e il successivo di smontaggio della directory NFS. Si veda anche il parametro ${BOLD}volte_umount${NORM} ${BOLD}[$NFS_UMOUNT_TMOUT]${NORM}
${BOLD}filesystem${NORM} indica il filesystem di cui eseguire il dump, indicato o come punto di montaggio di un dispositivo o come nome di dispositivo; se non indicato, la procedura interesser� tutti i filesystem che necessitano di un dump ${BOLD}[NON ANCORA IMPLEMENTATO!]${NORM}
${BOLD}parms${NORM} parametri arbitrari da passare al comando dump
${BOLD}-e${NORM} cessa la continuazione dello script se dump termina con un errore
${BOLD}-h${NORM} help, mostra questo testo
${BOLD}-N${NORM} se il filesystem NFS � stato montato con l'opzione ${BOLD}-m${NORM}, non deve essere automaticamente smontato a fine esecuzione ${BOLD}[$(test "$NFS_AUTOUMOUNT" = 'yes' && echo 'smonta' || echo 'non smontare')]${NORM}
${BOLD}-n${NORM} attiva la modalit� di funzionamento NFS in cui il file di dump � scritto in una named pipe ${BOLD}[$NFS]${NORM}
${BOLD}-o${NORM} indica che il file di output, se gi� esistente, deve essere sovrascritto ${BOLD}[$OVERWRITE]${NORM}
${BOLD}-q${NORM} attiva la modalit� quieta, in cui non sono prodotti messaggi ${BOLD}[$(test "$QUIET" -ne 0 && echo 'normale' || echo 'quieta')]${NORM}
${BOLD}-s${NORM} mantieni sincronizzati i file dumpdates, quello dello script ${BOLD}"$DUMPDATES"${NORM} e quello di sistema ${BOLD}[$(test "$SYNCDD" = 'true' && echo 'si' || echo 'no')]${NORM}
${BOLD}-t${NORM} crea il file indice (toc: table of contents) ${BOLD}[$TOC]${NORM}
${BOLD}-T${NORM} modalit� di test: il comando dump non � mandato in esecuzione, ma solo scritto sul terminale ${BOLD}[$TEST]${NORM}
${BOLD}-v${NORM} attiva la modalit� verbosa ${BOLD}[$VERB]${NORM}
${BOLD}-V${NORM} mostra informazioni su: versione, autore e licenza
${BOLD}-x${NORM} trace execution, esecuzione in modalit� tracciamento delle funzioni di shell
EOH
}

function VerificaHostname () {
# Controllo approssimativo, in realt� la RFC-1034 � pi� esigente
if test $# -eq 1
then A="$(echo $1 | tr -cd 'A-Za-z0-9.-')"
     if test "$1" != "$A"
     then return 10
     fi
else QEcho "${BOLD}ERRORE:${NORM} funzione VerificaHostname invocata con $# parametri invece di 1" >&2
     return 15
fi
}

function VerificaNumero () {
# Controlla che il parametro fornito sia un numero decimale
# Codici di errore: 20 = parametro non � un numero decimale
#                   22 = errore in fase di esecuzione di test
#                   25 = numero di parametri errato
if test $# -eq 1
then if test "${#1}" -le 3 # Numeri oltre le centinaia non hanno senso
     then if ! [[ "$1" =~ ^[0-9]+$ ]]
          then return 20
          fi
     else return 22
     fi
else QEcho "${BOLD}ERRORE:${NORM} funzione VerificaNumero invocata con $# parametri invece di 1" >&2
     return 25
fi
}

function VerificaNumero_err () {
# Il chiamante si prende cura dell'exit: chiamanti vari potrebbero volere un codice di uscita diverso
if test $# -eq 2
then if ! [[ "$1" =~ ^[0-9]+$ ]]
     then case $1 in
               20|22) QEcho "${BOLD}ERRORE:${NORM} indicazione $2 non valida: dev'essere un numero tra 0 e 999 (trovato: $OPTARG)" >&2
                      ;;
               25) QEcho "${BOLD}ERRORE:${NORM} procedura interna eseguita con numero di parametri errato" >&2
                   exit $1 ;;
               *) QEcho "${BOLD}ERRORE:${NORM} procedura interna fallita per motivi ignoti (VerificaNumero_err)" >&2
                  exit 103 ;;
          esac
     else QEcho "${BOLD}ERRORE:${NORM} funzione VerificaNumero_err invocata con il primo parametro non numerico" >&2
          return 27
     fi
else QEcho "${BOLD}ERRORE:${NORM} funzione VerificaNumero_err invocata con $# parametri invece di 2" >&2
     return 26
fi
}

function VerificaData () {
if test $# -eq 1
then if [[ "$DATE" =~ ^[12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]$ ]]
     then if ! date "--date=$DATE" '+%Y-%m-%d' &> /dev/null
          then return 30
	  fi
     else return 31
     fi
else QEcho "${BOLD}ERRORE:${NORM} funzione VerificaData invocata con $# parametri invece di 1" >&2
     return 35
fi
}

function ChkDir () {
if test $# -eq 1
then if test ! -d "$1" -o ! -w "$1"
     then QEcho "${BOLD}ERRORE:${NORM} il parametro \"$1\" non � una directory o non � scrivibile" >&2
	  return 50
     fi
else QEcho "${BOLD}ERRORE:${NORM} funzione ChkDir invocata con $# parametri invece di 1" >&2
     return 37
fi
}

function FindDumpdates () {
# Tentativi che fanno via via pi� affidamento a criteri probabilistici o di verosimiglianza per 
# capire se il comando dump del sistema in uso usa di default il file dumpdates in /etc oppure in
# /var/lib. Debian 8.3 e Ubuntu 16.04 usano di default /var/lib/dumpdates; Fedora23 (e,
# suppongo, RedHat, CentOS, Mandriva e YellowDog) invece usano (correttamente :-) /etc/dumpdates
DISTRO=''
DDFILE=''
if command -v lsb_release > /dev/null
then DISTRO="$(lsb_release --short --id)"
     # Possibili valori noti: Debian, Fedora, Ubuntu
     # Possibili valori pi� o meno tirati ad indovinare: CentOS, MandrivaLinux, YellowDog
     # Le altre distribuzioni non le prendo in considerazione perch� non ho idea di dove
     # abbiano il file dumpdates
fi
if test -z "$DISTRO"
then # O non abbiamo a disposizione il comando lsb_release, oppure non ne siamo riusciti ad estrarre
     # il nome della distribuzione. Tentiamo allora di cercare un file in /etc/ che ci dica in che
     # distribuzione stiamo girando, NON QUALSIASI distribuzione, ma quelle di cui conosco il
     # comportamento di default di dump, ossia Debian6.0/Ubuntu11.10 e Fedora16.  Il dump delle prime
     # ha il file dumpdates di default in /var/lib/, mentre la seconda (e verosimilmente anche 
     # RedHat, SuSE/Novell, CentOS, Mandriva, YellowDog ecc.) ce l'ha in /etc/.
     DISTROFILE=($(find /etc -maxdepth 1 -type f \( -name '*release' -o -name '*version' \) ))
     # Possono essere pi� file: Debian6.0 ha /etc/debian_version, 
     # Ubuntu 16.04 ha /etc/debian_version e /etc/lsb-release, di contenuto diverso.
     # Fedora 16 ha /etc/fedora-release.
     # Non ne analizziamo il contenuto, ci limitiamo a considerare il nome del file, se presente.
     # Pi� righe?  Le analizziamo tutte!
     for (( I=0 ; I < ${#DDFILE[@]} ; I++ ))
     do RIGA="${DISTROFILE[$I]#/etc/}"
        for DISTRIBUTION in 'debian' 'fedora' 'redhat' 'mandriva' 'yellowdog'
        do if test [[ "${RIGA}" = "{DISTRIBUTION}"* ]]
	   then DISTRO="$DISTRIBUTION"
	        break
	   fi
	done
     done
else DISTRO="$(echo "$DISTRO" | tr '[[:upper:]]' '[[:lower:]]')" # Nel caso usassimo la codifica UTF-8
fi
if test -n "$DISTRO"
then # Evviva!  Abbiamo trovato la distribuzione!
     if test "$DISTRO" = 'debian' -o "$DISTRO" = 'ubuntu'
     then DDFILE='/var/lib/dumpdates'
     else if test "$DISTRO" = 'fedora' -o "$DISTRO" = 'redhat' -o "$DISTRO" = 'mandriva' -o "$DISTRO" = 'yellowdog'
          then DDFILE='/etc/dumpdates'
	       # Per tutte le altre distribuzioni di cui non so, DDFILE resta vuoto
	  fi
     fi
fi
if test -z "$DDFILE"
then # Niente, non si riesce a capire quale dumpdates dump usi a partire dal nome della
     # distribuzione.  E se cercassimo di farlo spifferare dallo stesso dump?  Ma dump non
     # ha un'opzione di aiuto che glielo faccia dire con le buone.  Dobbiamo tentare con le
     # cattive, se possibile.  strings fa parte del pacchetto binutils, che � opzionale,
     # non si dia quindi per scontato che � disponibile:
     if command -v strings > /dev/null
     then STRINGS=($(strings "$(which dump)" | grep '/dumpdates')) #"
          for (( I=0 ; I < ${#STRINGS[@]} ; I++ ))
	  do if [[ "$STRINGS[$I]" = */etc/dumpdates* ]]
	     then DDFILE='/etc/dumpdates'
	          break
	     else if [[ "$STRINGS[$I]" = */var/lib/dumpdates* ]]
	          then DDFILE='/var/lib/dumpdates'
		       break
		  fi
	     fi
	  done
     fi
fi
if test -z "$DDFILE"
then # Uffa.  Fin'ora tutto vano.  Vediamo se esiste gi� uno, e solo uno, dei file 
     # /etc/dumpdates o /var/lib/dumpdates/dumpdates.  Se si, assumo sia quello giusto.
     if test -f '/etc/dumpdates' -a ! -f '/var/lib/dumpdates'
     then DDFILE='/etc/dumpdates'
     else if test -f '/var/lib/dumpdates' -a ! -f '/etc/dumpdates' 
          then DDFILE='/var/lib/dumpdates'
	  fi
     fi
fi
# DDFILE contiene il percorso del file dumpdates trovato oppure una stringa vuota.
# Non � detto che DDFILE esista, fosse anche quello giusto, e non � detto che, anche
# esistesse, che sia un file e che sia quello giusto.
}

#########################################################
##  Analisi parametri e opzioni sulla riga di comando  ##
#########################################################

#shopt -s extglob # Non serve
# OPTIND=1 # Fatto automaticamente dalla shell al caricamento dello script
# Non usare shift [n]! Altrimenti getopts non vede le opzioni/parametri successivi!
OPTERR=0 # Resta silenzioso in caso di errori
while getopts :c:H:d:D:f:F:l:m:p:ehonNpqstu:TvVx NEXTOPT
do case "$NEXTOPT" in
        'c') if VerificaNumero "$OPTARG"
	     then NFS_UMOUNT_COUNT="$OPTARG"
             else VerificaNumero_err $? 'smontaggi NFS'
                  exit 28
             fi
	     ;;
        'H') # Controlla che parametro sia composto solamente di caratteri [a-zA-Z0-9.-]
	     if VerificaHostname "$OPTARG"
	     then HOST="$OPTARG"
	          # Aggiorniamo le variabili che dipendono da HOST: FILE e, se necessario, DUMPDATES
		  FILE="${HOST}_${DATE}_$LEVEL"
		  test "$USER_PROVIDED_DUMPDATES" = 'false' && DUMPDATES="$DIR/$DUMPDATES_FILE"
	     else case $? in 
	       	       10) ERR=$?
			   QEcho "${BOLD}ERRORE:${NORM} caratteri non validi nel nome host: $OPTARG" >&2 ;;
		       15) ERR=$?
			   QEcho "${BOLD}ERRORE:${NORM} procedura interna fallita" >&2 ;;
			*) QEcho "${BOLD}ERRORE:${NORM} procedura interna inaspettatamente fallita (VerificaHostname)" >&2
			   ERR=100 ;;
		   esac
		   exit "$ERR"
	     fi
	     ;;
        'd') DIR="$OPTARG"
	     # Non compierne verifiche: potrebbe stare sotto il possibile punto di montaggio (opzione -m)
	     # Aggiorno le variabili che dipendono da DIR
	     DUMPDATES="$DIR/$DUMPDATES_FILE"
	     ;;
	'e') QUITONERR='true' ;;
        'D') if VerificaData "$OPTARG"
	     then DATE="$OPTARG"
	          # Aggiorniamo la variabile FILE che dipende da DATE
		  FILE="${HOST}_${DATE}_$LEVEL"
	     else case $? in
	               30) ERR=$?
			   QEcho "${BOLD}ERRORE:${NORM} la data non � congrua con il calendario" >&2 ;;
		       31) ERR=$?
			   QEcho "${BOLD}ERRORE:${NORM} la data non � nel formato ANNO-ME-GI" >&2 ;;
		       *) QEcho "${BOLD}ERRORE:${NORM} procedura interna inaspettatamente fallita (VerificaData)" >&2
			  ERR=101 ;;
	          esac
		  exit "$ERR"
	     fi ;;
        'f') FILE="$OPTARG" ;;
	'F') DUMPDATES_FILE="$(basename "$OPTARG")" # "
	     # ATTENTO!  Se OPTARG=/dir1/dir2/, basename "$OPTARG" = dir2 e dirname "$OPTARG" = /dir1
	     # Questo non � un problema perch� l'opzione prevede che il percorso sia opzionale, ma la
	     # indicazione del file no, ci deve essere
	     if test "$DUMPDATES_FILE" = "$OPTARG" # � stato specificato solo un nome di file, non un percorso?
	     then DUMPDATES="$DIR/$DUMPDATES_FILE"
	          # Come per l'opzione -d DIR non se ne compiono verifiche: potrebbe stare sotto il possibile punto di montaggio (opzione -m)
	     else # � stato specificato anche il percorso, che sar� controllato dopo l'eventuale montaggio
		  DUMPDATES="$OPTARG"
	     fi
	     USER_PROVIDED_DUMPDATES='true' ;;
	'l') # Controlla che il parametro sia composto solamente di caratteri numerici
	     if VerificaNumero "$OPTARG"
	     then LEVEL="$OPTARG"
	          # Aggiorniamo la variabile FILE che dipende da LEVEL
		  FILE="${HOST}_${DATE}_$LEVEL"
	     else VerificaNumero_err $? 'livello dump'
                  exit $?
	     fi ;;
	'm') if test -d "$OPTARG"
	     then MOUNTDIR="$OPTARG"
	     else QEcho "${BOLD}ERRORE:${NORM} il parametro non indica una directory: $OPTARG" >&2
	          exit 36
	     fi
	     ;;
        'n') NFS='yes' ;;
        'N') NFS_AUTOUMOUNT='no' ;;
        'o') OVERWRITE='yes' ;;
        'p') PARMS="$OPTARG" ;;
	'q') QUIET=0 ; VERB='no' ;;
	's') SYNCDD='true' ;;
        't') TOC='yes' ;;
	'T') TEST='yes'
	     ECHO='echo' ;;
        'h') Help
	     if test "$VERB" = 'yes'
	     then echo ; Codici
	     fi
	     exit ;;
        'u') if VerificaNumero "$OPTARG"
	     then NFS_UMOUNT_TMOUT="$OPTARG"
             else VerificaNumero_err $? 'attesa smontaggio NFS'
                  exit 29
             fi
	     ;;
        'v') VERB='yes' ; QUIET=1 ;;
	'V') Versione ; exit ;;
	'x') set -o xtrace ;;
	# Questo � eseguito quando getopts incontra un'opzione non prevista ma sintatticamente
	# valida
        *) # Quando OPTERR=0 NEXTOPT � posto a ? e in OPTARG compare l'opzione non valida
	   # riscontrata; ne indico anche il numero ordinale di opzione
	   QEcho "${BOLD}ERRORE:${NORM} opzione numero $((OPTIND - 1)) ($OPTARG) non valida" >&2
	   exit 1 ;;
   esac
done

##############################################################
##  Fine analisi parametri e opzioni sulla riga di comando  ##
##############################################################

# Eseguo qui i controlli sulla scrivibilit� delle cartelle dopo l'eventuale montaggio, non
# nella procedura di analisi opzioni, perch� questi controlli devono essere indipendenti dal_
# l'ordine delle opzioni sulla riga di comando (-d DIR -m MOUNTDIR oppure -m MOUNTDIR -d DIR)
# e DIR potrebbe essere una sottocartella di MOUNTDIR.  Inoltre fosse questa esecuzione un
# test allora non sarebbe montata MOUNTDIR, per cui non ne vedrei le sottocartelle e il
# controllo sulla loro scrivibilit� fallirebbe
MD_IN_FSTAB="$(awk -v V="$MOUNTDIR" '$1 !~ /^#/ && $2 == V {FOUND = 1 ; exit} END {print FOUND ? "yes" : "no"}' /etc/fstab)"

if test -n "$MOUNTDIR" -a "$(Md_Mounted)" = 'no'
then # MOUNTDIR � definita (e quindi da montare) e non � montata
     if test "$MD_IN_FSTAB" = 'yes'
     then $ECHO mount "$MOUNTDIR"
	  ERR=$?
	  if test $ERR -ne 0
	  then QEcho "${BOLD}ERRORE:${NORM} montaggio di \"$MOUNTDIR\" fallito con errore num. $ERR" >&2
	       exit 39
	  fi
     else QEcho "${BOLD}ERRORE:${NORM} montaggio di \"$MOUNTDIR\" impossibile: la directory non compare in /etc/fstab" >&2
	  exit 38
     fi
fi

# O MOUNTDIR non � definita (e quindi non � da montare), o era stata montata prima
# che fosse avviata l'esecuzione dello script, oppure � stata montata poche righe in alto.
# Controlla che DIR sia scrivibile e aggiorna DUMPDATES
ChkDir "$DIR"
ERR=$?
if test $ERR -eq 0
then # Aggiorniamo, se necessario, la variabile DUMPDATES che dipende da DIR
     test "$USER_PROVIDED_DUMPDATES" = 'false' && DUMPDATES="$DIR/$DUMPDATES_FILE"
else QEcho "${BOLD}ERRORE:${NORM} \"$DIR\" non indica una directory scrivibile" >&2
     exit 50
fi
# Adesso si controlla la directory di DUMPDATES
### Da controllare! ###
ChkDir "$(dirname "$DUMPDATES")"
ERR=$?
if test $ERR -ne 0
then QEcho "${BOLD}ERRORE:${NORM} \"$(dirname "$DUMPDATES")\" non indica una directory scrivibile" >&2
     exit 53
fi

if test -z "$TMPDIR" -o ! -d "$TMPDIR"
then if test -z "$TMP" -o ! -d "$TMP"
     then VEcho "Variabili TMPDIR e TMP non inizializzate o che non puntano ad una directory." >&2
          VEcho "Posto TMPDIR=/tmp" >&2
          TMPDIR='/tmp'
     fi
fi

if test "$NFS" = 'yes'
then if PIPE="$(mktemp -u --tmpdir="$TMPDIR")"
     then $ECHO mknod "$PIPE" p
          if test $? -ne 0
          then QEcho "${BOLD}ERRORE:${NORM} fallita creazione di named pipe in $TMPDIR" >&2
               exit 45
          fi
     else QEcho "${BOLD}ERRORE:${NORM} fallita esecuzione di mktemp" >&2
          exit 40
     fi
fi

# Elencare i filesystem di cui fare il dump
MFS=($(awk '$1 !~ /^#/ && NF == 6 && $5 != 0 {print $2}' /etc/fstab))
if test ${#MFS[@]} = 0
then QEcho "${BOLD}ATTENZIONE:${NORM} non � stato trovato nessun filesystem di cui eseguire il dump in /etc/fstab" >&2
     exit 3
fi

# Si procede ora con il dump
IND=0
while test "$IND" -lt "${#MFS[@]}"
do FS="${MFS[$IND]}"
   if test -d "$FS"
   then if test "$FS" = '/'
        then FILESYSTEM='_rootfs'
        else FILESYSTEM="$(echo "$FS" | tr '/' '_')"
        fi
        DUMPFILE="$DIR/${FILE}${FILESYSTEM}.dump"
	if test "$OVERWRITE" = 'yes' -o ! -e "$DUMPFILE"
        then TOCFILE="${DUMPFILE%.dump}.toc"
	     if test "$NFS" = 'yes'
             then if test "$TEST" = 'yes'
	          then echo "cat $PIPE > $DUMPFILE &"
		  else cat "$PIPE" > "$DUMPFILE" &
		  fi
                  DUMPFILE="$PIPE"
             fi
	     if test "$TOC" = 'yes' -a \( "$OVERWRITE" = 'yes' -o ! -e "$TOCFILE" \)
	     then MKTOC='-A'
	     else MKTOC=''
	     fi
	     # stringhe vuote come argomento confondono dump che non capisce di quale filesystem fare il dump
	     if test "$QUIET" -eq 0
	     then # Controlla che non si possano fare tutte e due le righe sottostanti con un eval ###
	          $ECHO dump -D "$DUMPDATES" "-u${LEVEL}f" "$DUMPFILE" ${MKTOC:+$MKTOC"$TOCFILE"} $PARMS "$FS" &> /dev/null
	          ERR=$?
	     else $ECHO dump -D "$DUMPDATES" "-u${LEVEL}f" "$DUMPFILE" ${MKTOC:+$MKTOC"$TOCFILE"} $PARMS "$FS"
	          ERR=$?
	     fi
	     if test "$QUITONERR" = 'true' -a $ERR -ne 0
	     then QEcho "${BOLD}ERRORE:${NORM} comando dump terminato con errore $ERR" >&2
	          exit 70
	     else VEcho "${BOLD}NOTA:${NORM} comando dump terminato con codice $ERR" >&2
	     fi
	else QEcho "${BOLD}IMPORTANTE:${NORM} la sovrascrittura non � attivata e il file \"$DUMPFILE\" esiste." >&2
	     QEcho "Dump saltato per il filesystem \"$FS\"" >&2
	fi
   else QEcho "${BOLD}ERRORE:${NORM} il mountpoint \"$FS\" non � una directory. Controllare il file /etc/fstab" >&2
   fi
IND=$((IND+1))
done

if test "$SYNCDD" = 'true'
then alias Err_dump='QEcho "${BOLD}AVVISO:${NORM} sincronizzazione dei file dumpdates non effettuata anche se richiesta;" >&2'
     FindDumpdates
     if test $? -eq 0 -a -n "$DDFILE"
     then if test -f "$DDFILE"
          then # il 2> /dev/null forse dovrebbe essere fatto solo quando QUIET=1?
	       cp -f "$DDFILE"{,.bak}
	       ERR=$?
	       if test $ERR -eq 0
	       then $ECHO cp -f "$DUMPDATES" "$DDFILE" 2> /dev/null
                    ERR=$?
                    if test $ERR -eq 0
	            then VEcho "Copia del file ${BOLD}$DUMPDATES${NORM} in ${BOLD}$DDFILE${NORM} effettuata"
	            else Err_dump
		         QEcho "  ${BOLD}AVVISO:${NORM} copia di ${BOLD}$DUMPDATES${NORM} in ${BOLD}$DDFILE${NORM} terminata con codice $ERR" >&2
	            fi
	       else Err_dump
	            QEcho "${BOLD}AVVISO:${NORM} la copia di ${BOLD}$DDFILE${NORM} in ${BOLD}${DDFILE}.bak${NORM} uscita con codice $ERR" >&2
	       fi
	  else Err_dump
	       QEcho "  l'oggetto \"dumpdates\" rilevato dalla funzione ${BOLD}FindDumpdates${NORM} non � un file" >&2
	  fi
     else Err_dump
          QEcho "  la funzione ${BOLD}FindDumpdates${NORM} non ha potuto rilevare il file dumpdates di sistema" >&2
     fi
else VEcho "Copia di sincronizzazione del file dumpdates non richiesta"
fi
